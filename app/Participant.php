<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
      public $timestamps = false;
      protected $fillable = ['name','email','type_document','document','last_name','phone'];
}
