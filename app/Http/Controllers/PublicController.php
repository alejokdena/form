<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Participant;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Support\Facades\Hash;
use Ixudra\Curl\Facades\Curl;

class PublicController extends Controller
{
    public function show_form()
    {
    	return View('form');
    }
    public function register(Request $request)
    {

        $request->validate([
            'name'=>['required','min:2','max:255'],
            'last_name'=>['required','min:2','max:255'],
            'phone' =>['required', 'numeric'],
            'email'=> ['required','email','max:255','unique:participants,email'],
            'document'=> ['required', 'numeric','unique:participants,document']
        ]);
    	$data = ['name' => $request['name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'type_document' => $request['type_document'],
            'document' => $request['document'],
            'phone' => $request['phone']
        ];
        $participant = Participant::create($data);        
        if($participant){
            $participants = Participant::get();
            return view("list", ['participants' => $participants ]);
        } else {
            return ['success' => false];
        }
    }
    public function show_register()
    {
        return View('list');
    }
}
