<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Formulario!</title>
  </head>
  <body>
    <h1>Solicita más información</h1>

    @if ($errors->any())
      @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
              {{ $error }}
          </div>
      @endforeach
        
    @endif

    <form method="POST" action="{{ route('save-register') }}">
      <div class="form-group">
        <label for="name">Nombres</label>
        <input type="text" name="name" class="form-control" id="name"  placeholder="Escribe tu nombre" required>
      </div>
      <div class="form-group">
        <label for="last_name">Apellidos</label>
        <input type="text" name="last_name" class="form-control" id="last_name"  placeholder="Escribe tu apellido" required>
      </div>
      <div class="form-group">
        <label for="email">Email </label>
        <input type="email" name="email" class="form-control" id="email" placeholder="Escribe tu email" required>
      </div>
      <div class="form-group">
        <label for="type_document">Tipo de Documento</label>
        <select class="form-control" name="type_document" id="type_document" required>
          <option value="CC">Cedula</option>
          <option value="CE">Cedula de extranjeria</option>
        </select>
      </div>
      
      <div class="form-group">
        <label for="document">Documento</label>
        <input type="text" name="document" class="form-control" id="document"  placeholder="Escribe tu documento" required>
      </div>
      <div class="form-group">
        <label for="document">Telefono</label>
        <input type="text" name="phone" class="form-control" id="phone"  placeholder="Escribe tu telefono" required>
      </div>
      <button type="submit" class="btn btn-primary">Enviar</button>
    </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>