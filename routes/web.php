<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('form','PublicController@show_form');
Route::get('list','PublicController@show_register');

//Route::post('form-save','PublicController@form_save')->name('form-save');
Route::post('register-participant', 'PublicController@register')->name('save-register');
